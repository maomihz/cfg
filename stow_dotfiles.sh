#!/usr/bin/env bash
set -xe

stow dotfiles --adopt --dotfiles

if [ -d secrets ]; then
  stow secrets --adopt --no-folding --dotfiles
fi
