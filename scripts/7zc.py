"""
This script creates a 7z archive from one or more specified directories or files.
It uses the 7z command-line tool to create the archive.
The archive name is constructed using a prefix, the name of the first path provided, a suffix, and optionally the last modified date of the first path.

The script can be executed from the command line as follows:

python script_name.py <path1> <path2> ... [options]

Where:
    <path1> <path2> ... are the paths to the directories or files to archive.
    [options] are optional arguments, including:
        --prefix: A prefix for the archive name (default: "").
        --suffix: A suffix for the archive name (default: "").
        --no-date: Do not append the modified date to the archive name.
        --compression-level: The compression level to use (0-9, default: 0).

For example:

python script_name.py my_folder --prefix backup_ --suffix _files --compression-level 9

This will create an archive named backup_my_folder_files_YYYYMMDD.7z (where YYYYMMDD is the last modified date of my_folder)
with the highest compression level.

python script_name.py file1.txt file2.txt /my/folder --no-date

This will create an archive named file1.txt.7z (or the name of the first file if it's a different one) with no date appended
and no compression.
"""

import argparse
from pathlib import Path
import subprocess
from datetime import datetime

def create_archive(paths, prefix, suffix, append_date, compression_level):
    """
    Creates a 7z archive from the given paths.

    Args:
        paths: A list of directory or file paths to include in the archive.
        prefix: A prefix for the archive name.
        suffix: A suffix for the archive name.
        append_date: A boolean indicating whether to append the modified date to the archive name.
        compression_level: The compression level to use (0-9).
    """
    # Ensure at least one path is provided
    if not paths:
        raise ValueError("At least one directory or file path must be specified.")

    # Resolve the first path for naming purposes
    first_path = Path(paths[0])
    if not first_path.exists():
        raise FileNotFoundError(f"The path {first_path} does not exist.")

    # Extract name and modified date from the first path
    base_name = first_path.name
    date_str = ""
    if append_date:
        mod_time = datetime.fromtimestamp(first_path.stat().st_mtime)
        date_str = f"_{mod_time.strftime('%Y%m%d')}"

    # Construct the archive name
    archive_name = f"{prefix}{base_name}{suffix}{date_str}.7z"
    archive_path = Path(archive_name)

    # Check if the archive already exists
    if archive_path.exists():
        print(f"The archive {archive_path} already exists. Skipping.")
        return

    # Prepare the 7z command
    command = [
        "7z", "a", "-t7z", f"-mx{compression_level}", "-mtr-", str(archive_path)
    ]
    command.extend(map(str, paths))  # Add all provided paths

    # Run the command using subprocess
    try:
        subprocess.run(command, check=True)
        print(f"Archive created successfully: {archive_path}")
    except subprocess.CalledProcessError as e:
        print(f"An error occurred while creating the archive: {e}")
        exit(1)

def main():
    parser = argparse.ArgumentParser(description="Create a .7z archive from specified directories or files.")
    parser.add_argument(
        "paths",
        metavar="PATH",
        type=str,
        nargs="+",
        help="One or more directories or files to archive.",
    )
    parser.add_argument(
        "--prefix",
        type=str,
        default="",
        help="Prefix for the archive name.",
    )
    parser.add_argument(
        "--suffix",
        type=str,
        default="",
        help="Suffix for the archive name.",
    )
    parser.add_argument(
        "--no-date",
        action="store_true",
        help="Do not append the modified date to the archive name.",
    )
    parser.add_argument(
        "--compression-level",
        type=int,
        default=0,
        choices=range(0, 10),
        help="Compression level to use (0-9, default: 0).",
    )
    args = parser.parse_args()

    append_date = not args.no_date

    # Call the function to create the archive
    create_archive(args.paths, args.prefix, args.suffix, append_date, args.compression_level)

if __name__ == "__main__":
    main()
