# bitflip.py: Destroy 1 bit of the file

import os
from sys import argv
from random import randint

for file_name in argv[1:]:
    # Check is a valid file
    if not os.path.isfile(file_name):
        print(file_name, 'is not a file!')
        continue

    # Check the file size > 0
    file_size = os.path.getsize(file_name)
    if file_size <= 0:
        continue

    # Generate a random offset
    offset = randint(0, file_size - 1)
    with open(file_name, 'rb+') as f:
        f.seek(offset)
        data = int.from_bytes(f.read(1))
        print(f"{file_name}\t{offset}\t{data}")
        x = 1 << randint(0, 7)
        f.seek(offset)
        f.write((data ^ x).to_bytes())
