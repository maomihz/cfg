"""
This script provides functionality to flatten file names by appending
various forms of timestamp information or to restore file modification
times based on their names. It supports moving or copying files to
the current directory and includes a dry-run mode for simulation.

Main Features:
1. Flatten file names by appending:
   - UNIX timestamp (--timestamp)
   - Modified date (--date)
   - Modified datetime (--datetime)
2. Restore file modification times from filenames (--restore).
3. Dry-run mode to simulate operations (--dry-run).
4. Optionally copy files instead of moving them (--copy).

Usage Examples:
- Flatten files with a timestamp:
  python script.py file1.txt -t

- Restore modification times from filenames:
  python script.py file1_20220101.txt -r

- Perform a dry run:
  python script.py file1.txt -t -n

"""

import argparse
from pathlib import Path
from datetime import datetime
import os
import shutil

class FileFlattenRestore:
    """
    A class to flatten file names by appending timestamp information or restore
    file modification times based on their names. Supports dry-run mode and file copying.
    """

    def __init__(self, append_timestamp: bool = False, append_date: bool = False,
                 append_datetime: bool = False, restore: bool = False, dry_run: bool = False,
                 copy_file: bool = False, no_rename: bool = False):
        self.append_timestamp = append_timestamp
        self.append_date = append_date
        self.append_datetime = append_datetime
        self.restore = restore
        self.dry_run = dry_run
        self.copy_file = copy_file
        self.no_rename = no_rename

    def flatten_file(self, file_path: Path, target_dir: Path):
        """Flatten the file name by appending timestamp, date, or datetime."""
        if not file_path.exists() or not file_path.is_file():
            print(f"Skipping: {file_path} is not a valid file.")
            return

        # Append timestamp, date, or datetime only if not already appended
        modified_time = datetime.fromtimestamp(file_path.stat().st_mtime)
        if self.no_rename:
            flattened_name = file_path.name
        else:
            file_ext = file_path.suffix  # File extension (e.g., .txt)
            # Flatten the file name using Path.parts for platform independence
            name_parts = file_path.parts[:-1]  # Exclude the actual file name
            base_name = "_".join(name_parts + (file_path.stem,))

            if self.append_timestamp:
                timestamp = int(modified_time.timestamp())
                if not base_name.endswith(f"_{timestamp}"):
                    base_name = f"{base_name}_{timestamp}"
            elif self.append_date:
                date = modified_time.strftime("%Y%m%d")
                if not base_name.endswith(f"_{date}"):
                    base_name = f"{base_name}_{date}"
            elif self.append_datetime:
                datetime_str = modified_time.strftime("%Y%m%d%H%M%S")
                if not base_name.endswith(f"_{datetime_str}"):
                    base_name = f"{base_name}_{datetime_str}"

            # Add the file extension
            flattened_name = f"{base_name}{file_ext}"

        target_path = target_dir / flattened_name

        if target_path.exists():
            print(f"File {target_path} already exists. Skipping to avoid overwrite.")
            return

        if self.dry_run:
            action = "copy" if self.copy_file else "move"
            print(f"[Dry Run] Would {action}: {file_path} -> {target_path}")
        else:
            if self.copy_file:
                shutil.copy2(file_path, target_path)  # copy2 preserves metadata
                print(f"Copied: {file_path} -> {target_path}")
            else:
                file_path.rename(target_path)
                print(f"Moved: {file_path} -> {target_path}")

    def restore_file_time(self, file_path: Path):
        """Restore the file modification time from the filename."""
        if not file_path.exists() or not file_path.is_file():
            print(f"Skipping: {file_path} is not a valid file.")
            return

        # Iterate through the filename parts in reverse to find the last appended timestamp/date/datetime
        name = file_path.name
        for name_part in reversed(name.split('.')):
            if name_part.rfind("_") > 0:
                str_to_convert = name_part[name_part.rfind("_") + 1:]
                if str_to_convert.isdigit():
                    break
        else:
            print(f"Skipping: {file_path}: Could not parse date/time from filename.")
            return

        extracted_time = None
        # Try to parse the date/datetime string
        try:
            if len(str_to_convert) == 8:
                # Date format
                extracted_time = datetime.strptime(str_to_convert, "%Y%m%d")
            elif len(str_to_convert) == 14:
                # Datetime format
                extracted_time = datetime.strptime(str_to_convert, "%Y%m%d%H%M%S")
        except ValueError:
            pass

        # If no date/time information is extracted, try parsing as a timestamp
        if not extracted_time:
            try:
                # Default to Timestamp format
                extracted_time = datetime.fromtimestamp(int(str_to_convert))
            except ValueError:
                print(f"Skipping: {file_path}: Could not parse date/time from filename ({str_to_convert}).")
                return

        # Convert extracted_time to epoch time
        extracted_timestamp = int(extracted_time.timestamp())

        if self.dry_run:
            print(f"[Dry Run] Would restore modification time of {file_path} to {extracted_time}")
        else:
            # Set the access and modification times of the file
            os.utime(file_path, (extracted_timestamp, extracted_timestamp))
            print(f"Restored modification time of {file_path} to {extracted_time}")

    def run(self, files: list[Path]):
        """Run the flattening or restoration process on the provided files."""
        current_dir = Path.cwd()
        for file in files:
            if self.restore:
                self.restore_file_time(file)
            else:
                self.flatten_file(file, current_dir)

def main():
    parser = argparse.ArgumentParser(description="Flatten or restore file modification times.")
    parser.add_argument("files", metavar="FILE", type=Path, nargs="+", help="List of files to process.")
    parser.add_argument("-t", "--timestamp", action="store_true", help="Append UNIX timestamp to filenames.")
    parser.add_argument("-d", "--date", action="store_true", help="Append modified date (YYYYMMDD) to filenames.")
    parser.add_argument("-D", "--datetime", action="store_true", help="Append modified datetime (YYYYMMDDHHMMSS) to filenames.")
    parser.add_argument("-n", "--dry-run", action="store_true", help="Simulate the operations without making changes.")
    parser.add_argument("-r", "--restore", action="store_true", help="Restore file modification times from filenames.")
    parser.add_argument("-c", "--copy", action="store_true", help="Copy files instead of moving them.")
    parser.add_argument("--no-rename", action="store_true", help="Bypass any file name changes when flattening.")

    args = parser.parse_args()

    if sum((args.timestamp, args.date, args.datetime)) > 1:
        print("Error: Only one of --timestamp, --date, or --datetime can be specified at a time.")
        return

    if args.restore and (args.timestamp or args.date or args.datetime or args.copy):
        print("Error: --restore cannot be used with --timestamp, --date, --datetime, or --copy.")
        return

    flattener = FileFlattenRestore(
        append_timestamp=args.timestamp,
        append_date=args.date,
        append_datetime=args.datetime,
        restore=args.restore,
        dry_run=args.dry_run,
        copy_file=args.copy,
        no_rename=args.no_rename
    )

    flattener.run(args.files)


if __name__ == "__main__":
    main()
