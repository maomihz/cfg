#!/bin/bash

# Define directories
ARCHIVE_DIR="$PWD"
PREFIX_DIR="$HOME/.local"
INSTALL_DIR="$PREFIX_DIR/opt"
BIN_DIR="$PREFIX_DIR/bin"

# Function to fetch the latest release version from GitHub
fetch_latest_version() {
    REPO=$1
    API_URL="https://api.github.com/repos/$REPO/releases/latest"
    curl -s "$API_URL" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | tr -d 'a-z'
}

# Function to update code-server
update_code_server() {
    local latest_version=$(fetch_latest_version "coder/code-server")
    local ARCHIVE_NAME="code-server-$latest_version-linux-amd64.tar.gz"
    local DOWNLOAD_URL="https://github.com/coder/code-server/releases/download/v$latest_version/$ARCHIVE_NAME"
    local INSTALLATION_PATH="$INSTALL_DIR/code-server-$latest_version-linux-amd64"

    echo "Checking for code-server updates..."
    if [ ! -d "$INSTALLATION_PATH" ]; then
        echo "Downloading code-server $latest_version..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        echo "Installing code-server $latest_version..."
        tar -xzf "$ARCHIVE_DIR/$ARCHIVE_NAME" -C "$INSTALL_DIR"
        ln -sfn "../opt/code-server-$latest_version-linux-amd64/bin/code-server" "$BIN_DIR/code-server"
        echo "code-server $latest_version installation complete."
    else
        echo "Latest version of code-server ($latest_version) is already installed."
    fi
}

# Function to update cloudflared
update_cloudflared() {
    local latest_version=$(fetch_latest_version "cloudflare/cloudflared")
    local EXECUTABLE_NAME="cloudflared-$latest_version"
    local DOWNLOAD_URL="https://github.com/cloudflare/cloudflared/releases/download/$latest_version/cloudflared-linux-amd64"

    echo "Checking for cloudflared updates..."
    if [ ! -f "$INSTALL_DIR/$EXECUTABLE_NAME" ]; then
        echo "Downloading cloudflared $latest_version..."
        curl -L "$DOWNLOAD_URL" -o "$INSTALL_DIR/$EXECUTABLE_NAME"
        chmod +x "$INSTALL_DIR/$EXECUTABLE_NAME"
    else
        echo "Latest version of cloudflared ($latest_version) already downloaded."
        return
    fi

    ln -sfn "../opt/$EXECUTABLE_NAME" "$BIN_DIR/cloudflared"
    echo "cloudflared $latest_version installation complete."
}

update_gdu() {
    local latest_version=$(fetch_latest_version "dundee/gdu")
    local ARCHIVE_NAME="gdu_linux_amd64.tgz"
    local DOWNLOAD_URL="https://github.com/dundee/gdu/releases/download/v$latest_version/$ARCHIVE_NAME"
    local EXECUTABLE_NAME="gdu-$latest_version"

    echo "Checking for gdu updates..."
    if [ ! -f "$INSTALL_DIR/$EXECUTABLE_NAME" ]; then
        echo "Downloading gdu $latest_version..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        tar -xzf "$ARCHIVE_DIR/$ARCHIVE_NAME" -C "$INSTALL_DIR"
        mv "$INSTALL_DIR/gdu_linux_amd64" "$INSTALL_DIR/$EXECUTABLE_NAME"
        chmod +x "$INSTALL_DIR/$EXECUTABLE_NAME"
    else
        echo "Latest version of gdu ($latest_version) already downloaded."
        return
    fi

    ln -sfn "../opt/$EXECUTABLE_NAME" "$BIN_DIR/gdu"
    echo "gdu $latest_version installation complete."
}

update_tmux() {
    local latest_version=$(fetch_latest_version "tmux/tmux")
    local TMUX_NAME="tmux-$latest_version"
    local DOWNLOAD_URL="https://github.com/tmux/tmux/releases/download/$latest_version/$TMUX_NAME.tar.gz"
    local ARCHIVE_NAME="$TMUX_NAME.tar.gz"
    local INSTALLATION_DIR="$INSTALL_DIR/$TMUX_NAME"

    echo "Checking for tmux updates..."
    if [ ! -d "$INSTALLATION_DIR" ]; then
        echo "Downloading tmux $latest_version..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        echo "Extracting tmux $latest_version..."
        tar -xzf "$ARCHIVE_DIR/$ARCHIVE_NAME" -C "$INSTALL_DIR"
        cd "$INSTALLATION_DIR"

        echo "Configuring tmux $latest_version..."
        ./configure --prefix="$PREFIX_DIR"

        echo "Compiling and installing tmux $latest_version..."
        make && make install
        echo "tmux $latest_version compilation and installation complete."
    else
        echo "Latest version of tmux ($latest_version) is already installed."
    fi
}

fetch_latest_tree_version() {
    # Replace PROJECT_ID with the actual numeric project ID or URL-encoded namespace/project_name
    local PROJECT_ID="34709077"  # OldManProgrammer/unix-tree
    local API_URL="https://gitlab.com/api/v4/projects/$PROJECT_ID/repository/tags"
    local LATEST_TAG=$(curl -s "$API_URL" | jq -r '.[0].name')
    
    # Assuming the tag name is the version. Adjust the cut delimiter/fields if the tag includes additional prefix/suffix.
    echo "${LATEST_TAG}"
}

update_tree() {
    local VERSION=$(fetch_latest_tree_version)
    local TREE_NAME="unix-tree-$VERSION"
    local DOWNLOAD_URL="https://gitlab.com/OldManProgrammer/unix-tree/-/archive/$VERSION/$TREE_NAME.tar.bz2"
    local ARCHIVE_NAME="$TREE_NAME.tar.bz2"
    local INSTALLATION_DIR="$INSTALL_DIR/$TREE_NAME"

    echo "Checking for tree updates..."
    if [ ! -d "$INSTALLATION_DIR" ]; then
        echo "Downloading tree version $VERSION..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        echo "Extracting tree version $VERSION..."
        tar -xjf "$ARCHIVE_DIR/$ARCHIVE_NAME" -C "$INSTALL_DIR"
        cd "$INSTALLATION_DIR"
        make
        echo "tree version $VERSION compilation complete."
        # Create or update symbolic link
        ln -sfn "../opt/$TREE_NAME/tree" "$BIN_DIR/tree"
        echo "Symbolic link created for tree version $VERSION."
    else
        echo "Latest version of tree ($VERSION) is already installed."
    fi
}

fetch_latest_zsh_version() {
    # Fetches the latest version tag from GitHub
    local LATEST_TAG=$(curl -s "https://api.github.com/repos/zsh-users/zsh/tags" | jq -r '.[0].name' | sed 's/zsh-//')
    echo "${LATEST_TAG}"
}

update_zsh() {
    local VERSION=$(fetch_latest_zsh_version)
    local ZSH_NAME="zsh-${VERSION}"
    local DOWNLOAD_URL="https://www.zsh.org/pub/${ZSH_NAME}.tar.xz"
    local ARCHIVE_NAME="${ZSH_NAME}.tar.xz"
    local INSTALLATION_DIR="$INSTALL_DIR/$ZSH_NAME"

    echo "Checking for Zsh updates..."
    if [ ! -d "$INSTALLATION_DIR" ]; then
        echo "Downloading Zsh version $VERSION..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        echo "Extracting Zsh version $VERSION..."
        tar -xJf "$ARCHIVE_DIR/$ARCHIVE_NAME" -C "$INSTALL_DIR"
        cd "$INSTALLATION_DIR"
        # Configure the directories. Adjust as necessary.
        ./configure --prefix="$PREFIX_DIR" --sysconfdir="$PREFIX_DIR/etc/zsh" --enable-etcdir="$PREFIX_DIR/etc/zsh" --enable-cap --enable-gdbm --enable-pcre
        echo "Compiling and installing Zsh version $VERSION..."
        make && make install
        echo "Zsh version $VERSION compilation and installation complete."
    else
        echo "Latest version of Zsh ($VERSION) is already installed."
    fi
}

update_rclone() {
    local VERSION=$(fetch_latest_version "rclone/rclone")
    local RCLONE_NAME="rclone-v${VERSION}-linux-amd64"
    local DOWNLOAD_URL="https://github.com/rclone/rclone/releases/download/v${VERSION}/${RCLONE_NAME}.zip"
    local ARCHIVE_NAME="${RCLONE_NAME}.zip"
    local UNZIP_DIR="$INSTALL_DIR/$RCLONE_NAME"

    echo "Checking for rclone updates..."
    # Checking the symlink's target existence is a way to see if update is needed
    if [ ! -d "$UNZIP_DIR" ]; then
        echo "Downloading rclone version $VERSION..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_DIR/$ARCHIVE_NAME"
        echo "Extracting rclone version $VERSION..."
        unzip -o "$ARCHIVE_DIR/$ARCHIVE_NAME" -d "$INSTALL_DIR"
        echo "Creating symlink for rclone version $VERSION..."
        # Ensure symlink points to the actual binary within the extracted directory
        ln -sfn "../opt/$RCLONE_NAME/rclone" "$BIN_DIR/rclone"
        echo "rclone version $VERSION installation complete."
    else
        echo "Latest version of rclone ($VERSION) is already installed."
    fi
}

update_restic() {
    local VERSION=$(fetch_latest_version "restic/restic")
    local RESTIC_NAME="restic_${VERSION}_linux_amd64"
    local DOWNLOAD_URL="https://github.com/restic/restic/releases/download/v${VERSION}/${RESTIC_NAME}.bz2"
    local ARCHIVE_NAME="${RESTIC_NAME}.bz2"
    local EXECUTABLE_PATH="$INSTALL_DIR/$RESTIC_NAME"

    echo "Checking for restic updates..."
    if [ ! -f "$EXECUTABLE_PATH" ]; then
        echo "Downloading restic version $VERSION..."
        curl -L "$DOWNLOAD_URL" -o "$ARCHIVE_NAME"
        echo "Extracting restic version $VERSION..."
        # Extract and move restic binary to the installation directory
        bzip2 -d "$ARCHIVE_NAME" && mv "${RESTIC_NAME}" "$EXECUTABLE_PATH"
        chmod +x "$EXECUTABLE_PATH"
        echo "Creating symlink for restic version $VERSION..."
        ln -sfn "$EXECUTABLE_PATH" "$BIN_DIR/restic"
        echo "restic version $VERSION installation complete."
    else
        echo "Latest version of restic ($VERSION) is already installed."
    fi
}




# Ensure installation directories exist
mkdir -p "$INSTALL_DIR" "$BIN_DIR"

# Check command-line argument
case "$1" in
    code-server)
        update_code_server
        ;;
    cloudflared)
        update_cloudflared
        ;;
    gdu)
        update_gdu
        ;;
    tmux)
        update_tmux
        ;;
    tree)
        update_tree
        ;;
    zsh)
        update_zsh
        ;;
    rclone)
        update_rclone
        ;;
    restic)
        update_restic
        ;;
    *)
        echo "Usage: $0 {code-server|cloudflared|gdu|tmux|tree|zsh|rclone|restic}"
        exit 1
        ;;
esac


