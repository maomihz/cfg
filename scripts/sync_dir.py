from pathlib import Path
import os
import re
import shutil
import subprocess
import argparse

ARCHIVE_SUFFIXES = ('.zip', '.7z', '.001', '.rar')
IGNORE_ARCHIVES = ('ind-mnd.zip',)

class DirectorySync:
    """
    A class to synchronize directories with optional archive extraction and handling
    of hidden or temporary files.
    """

    def __init__(self, input_dirs: list[Path], output_dir: Path, extract_archives: bool = True, copy_hidden: bool = False, no_create: bool = False, force: bool = False):
        self.input_dirs = input_dirs
        self.output_dir = output_dir
        self.extract_archives = extract_archives
        self.copy_hidden = copy_hidden
        self.no_create = no_create
        self.force = force

    def is_hidden(self, file_path: Path) -> bool:
        return file_path.name.startswith('.')

    def is_temp(self, file_path: Path) -> bool:
        return file_path.name.endswith('~') or file_path.name.endswith('.tmp')

    def is_archive(self, file_path: Path) -> bool:
        if file_path.name in IGNORE_ARCHIVES:
            return False
        return file_path.suffix.lower() in ARCHIVE_SUFFIXES

    def is_archive_volume(self, file_path: Path) -> bool:
        # .002 ~ .099 or .r00 ~ .z99
        match = re.match(r"[.](00[2-9]|0[1-9][0-9]|[r-z][0-9][0-9])$", file_path.suffix)
        return bool(match)

    def extract_archive(self, archive_path: Path, extracted_dir: Path):
        """Extract an archive file into the target directory using 7z."""
        try:
            subprocess.run(['7z', 'x', str(archive_path), f'-o{extracted_dir}'], check=True)
            print(f"Extracted archive: {archive_path} to {extracted_dir}")
        except subprocess.CalledProcessError as e:
            print(f"Failed to extract archive {archive_path}: {e}")
            raise

    def process_file(self, file_path: Path, dest_path: Path):
        """Process individual file for copying or extraction."""
        if self.extract_archives and self.is_archive(file_path):
            # Extract the archive to a directory of the same name
            extracted_dir = dest_path.with_suffix('')
            if extracted_dir.exists():
                print(f"Archive already extracted: {extracted_dir}")
                return
            extracted_dir.mkdir(parents=True, exist_ok=True)
            self.extract_archive(file_path, extracted_dir)
            # Copy the file modified time to extracted directory
            mod_time = file_path.stat().st_mtime
            os.utime(extracted_dir, (mod_time, mod_time))
        elif not dest_path.exists() or file_path.stat().st_mtime > dest_path.stat().st_mtime:
            # Synchronize the file based on modification time
            result = shutil.copy2(file_path, dest_path, follow_symlinks=False)
            print(f"Copied file: {file_path} to {result}")

    def process_directory(self, dir_path: Path, dest_path: Path):
        """Process a directory for synchronization."""
        dest_path.mkdir(parents=True, exist_ok=True)
        self.sync_directory(dir_path, dest_path)
        shutil.copystat(dir_path, dest_path)
        print(f"Updated directory modified time: {dest_path}")

    def sync_item(self, item: Path, output_base: Path):
        """Synchronize a single file or directory."""
        if (self.is_hidden(item) or self.is_temp(item)) and not self.copy_hidden:
            print(f"Skipping hidden or temporary item: {item}")
            return
        if self.extract_archives and self.is_archive_volume(item):
            print(f"Skipping archive volume: {item}")
            return

        dest_path = output_base / item.name
        if item.is_file():
            self.process_file(item, dest_path)
        elif item.is_dir():
            self.process_directory(item, dest_path)

    def sync_directory(self, input_dir: Path, output_base: Path):
        """Synchronize all items in a directory."""
        for item in input_dir.iterdir():
            self.sync_item(item, output_base)

    def run(self):
        """Run the directory synchronization process."""
        if self.no_create and len(self.input_dirs) != 1:
            print("Error: --no-create requires exactly one input directory.")
            return
        for src in self.input_dirs:
            target = self.output_dir if self.no_create else self.output_dir / src.name
            if target.exists() and not self.force:
                print(f"Skipping existing item: {target}. Use --force to overwrite.")
                continue
            if src.is_dir():
                self.process_directory(src, target)
                continue
            self.process_file(src, target)


def main():
    """Parse arguments and run the DirectorySync utility."""
    parser = argparse.ArgumentParser(description="Sync directories with archive extraction.")
    parser.add_argument("input_dirs", nargs='+', type=Path, help="Path(s) to the input directories.")
    parser.add_argument("-o", "--output-dir", type=Path, help="Path to the output directory.", default=Path.cwd())
    parser.add_argument("-x", "--extract-archives", action='store_true', help="Extract archives during sync.")
    parser.add_argument("-a", "--copy-hidden", action='store_true', help="Copy hidden files and directories.")
    parser.add_argument("--no-create", action='store_true', help="Do not create a new directory in the output directory.")
    parser.add_argument("-f", "--force", action='store_true', help="Force overwrite existing directories.")

    args = parser.parse_args()

    syncer = DirectorySync(
        input_dirs=args.input_dirs,
        output_dir=args.output_dir,
        extract_archives=args.extract_archives,
        copy_hidden=args.copy_hidden,
        no_create=args.no_create,
        force=args.force
    )
    syncer.run()


if __name__ == "__main__":
    main()
