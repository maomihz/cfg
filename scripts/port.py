#!/usr/bin/env python3

"""
This script generates random port numbers from predefined ranges (privileged, registered, or ephemeral).
Users can specify one or more ranges using command-line options and also choose how many random ports to generate.
By default, the script generates one ephemeral port if no options are provided.
"""

import random
import argparse
from typing import List

# Predefined port ranges
PORT_RANGES = {
    "privileged": (1, 1023),
    "registered": (1024, 49151),
    "ephemeral": (49152, 65535),
}

def generate_random_ports(selected_ranges: List[str], count: int) -> List[int]:
    """Generate a list of random ports from the specified ranges."""
    all_ports = []
    for range_name in selected_ranges:
        all_ports.extend(range(*PORT_RANGES[range_name]))

    return random.sample(all_ports, count)

def main():
    parser = argparse.ArgumentParser(
        description="Generate random port numbers from predefined ranges."
    )
    parser.add_argument(
        "-p", "--privileged",
        action="store_true",
        help="Include privileged ports (1-1023)."
    )
    parser.add_argument(
        "-g", "--registered",
        action="store_true",
        help="Include registered ports (1024-49151)."
    )
    parser.add_argument(
        "-e", "--ephemeral",
        action="store_true",
        help="Include ephemeral ports (49152-65535). Default if no range is specified."
    )
    parser.add_argument(
        "number",
        nargs="?",
        type=int,
        default=1,
        help="Number of random ports to generate (default: 1)."
    )

    args = parser.parse_args()

    # Determine selected ranges
    selected_ranges = set()
    if args.privileged:
        selected_ranges.add("privileged")
    if args.registered:
        selected_ranges.add("registered")
    selected_ranges.add("ephemeral") if not selected_ranges or args.ephemeral else None

    for port in generate_random_ports(selected_ranges, args.number):
        print(port)

if __name__ == "__main__":
    main()
